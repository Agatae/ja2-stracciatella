#ifndef STRATEGIC_MOVEMENT_COSTS_H
#define STRATEGIC_MOVEMENT_COSTS_H

#include "Types.h"

void InitStrategicMovementCosts();

UINT8 GetTraversability(SECTOR_ID startSector, SECTOR_ID endSector);

bool SectorIsPassable(SECTOR_ID sector);

#endif
